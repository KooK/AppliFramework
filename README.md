# AppliFramework

The main idea of the project is to have a standard way to start new program project.

All prog build with this framework should at least respond to those commands :
```
myapp.x -h/--help
myapp.x -v/--verbose
myapp.x --version
```
